package com.up.habit.expand.swagger;

import com.jfinal.core.Controller;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.template.Engine;
import com.up.habit.kit.RequestKit;


/**
 * TODO:
 * <p>
 * @author 王剑洪 on 2019/10/24 10:05
 */
public class SwaggerController extends Controller {
    public void index() {
        Engine engine = Engine.use().setToClassPathSourceFactory();
        String html = engine.getTemplate("/META-INF/resources/swagger/index.html")
                .renderToString(Kv.by("basePath", RequestKit.getHost(getRequest()))
                        .set("jsonAction", getRequest().getRequestURL().toString() + "/json" + (StrKit.isBlank(get(0)) ? "" : "/" + get(0))));
        renderHtml(html);
    }


    public void json() {
        String doc = SwaggerKit.getApiDoc(getRequest(), get(0) == null);
        getResponse().addHeader("Access-Control-Allow-Origin", "*");
        renderText(doc);

    }


}
