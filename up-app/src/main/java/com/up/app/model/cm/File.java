package com.up.app.model.cm;

import com.jfinal.plugin.activerecord.IBean;
import com.up.habit.expand.db.model.DBModel;

/**
 * 
 * @author 王剑洪
 */
@SuppressWarnings("serial")
public class File extends DBModel<File> implements IBean {
	/**表名*/
    public final static String TABLE_NAME="cm_file";
    /**主键*/
    public final static String TABLE_PKS="id";
    /**文件ID*/
    public final static String ID="id";
    /**文件路径*/
    public final static String PATH="path";
    /**1-图片,1-文件*/
    public final static String IMAGE="image";
    /**1-使用中*/
    public final static String USED="used";
    /***/
    public final static String CREATE_BY="create_by";
    /***/
    public final static String CREATE_TIME="create_time";
    /**swagger文档生成说明*/
    public final static String TABLE_INFO="<table style='font-size:14px; font-style:normal'>"
            +("<tr><td>id</td><td>文件ID</td></tr>")
            +("<tr><td>path</td><td>文件路径</td></tr>")
            +("<tr><td>image</td><td>1-图片,1-文件</td></tr>")
            +("<tr><td>used</td><td>1-使用中</td></tr>")
            +("<tr><td>create_by</td><td></td></tr>")
            +("<tr><td>create_time</td><td></td></tr>")
            +"</table>";

	/**
	 * 文件ID
	 */
	public File setId(Long id) {
		set("id", id);
		return this;
	}

	/**
	 * 文件ID
	 */
	public Long getId() {
		return getLong("id");
	}

	/**
	 * 文件路径
	 */
	public File setPath(String path) {
		set("path", path);
		return this;
	}

	/**
	 * 文件路径
	 */
	public String getPath() {
		return getStr("path");
	}

	/**
	 * 1-图片,1-文件
	 */
	public File setImage(Integer image) {
		set("image", image);
		return this;
	}

	/**
	 * 1-图片,1-文件
	 */
	public Integer getImage() {
		return getInt("image");
	}

	/**
	 * 1-使用中
	 */
	public File setUsed(Integer used) {
		set("used", used);
		return this;
	}

	/**
	 * 1-使用中
	 */
	public Integer getUsed() {
		return getInt("used");
	}

	/**
	 *
	 */
	public File setCreateBy(Integer createBy) {
		set("create_by", createBy);
		return this;
	}

	/**
	 *
	 */
	public Integer getCreateBy() {
		return getInt("create_by");
	}

	/**
	 * 
	 */
	public File setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
		return this;
	}

	/**
	 * 
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}


}
